var employees = [
    { name: "Bob", salary: 32000 },
    { name: "Alice", salary: 27000, endDate: new Date(2025, 0, 1), shouldBeRenewed: false }
];
var empTableBody = document.querySelector("tbody");
var _loop_1 = function (emp) {
    var tr = document.createElement("tr");
    var nameCell = document.createElement("td");
    var salaryCell = document.createElement("td");
    salaryCell.setAttribute("contenteditable", "true");
    salaryCell.onblur = function (evebt) {
        var inputedSalary = +salaryCell.innerText;
        /*emp.salary = !isNaN(inputedSalary)
        ? inputedSalary
        : (alert(salaryCell.innerText + " was not a valid number"), emp.salary)*/
        if (!isNaN(inputedSalary)) {
            emp.salary = inputedSalary;
        }
        else {
            alert(salaryCell.innerText + " was not a valid number");
        }
        salaryCell.innerText = emp.salary.toString();
    };
    var endDateCell = document.createElement("td");
    nameCell.append(document.createTextNode(emp.name));
    salaryCell.append(document.createTextNode(emp.salary.toString()));
    if ("endDate" in emp) {
        endDateCell.append(document.createTextNode(emp.endDate.toDateString()));
    }
    else {
        endDateCell.append(document.createTextNode("Tillsvidareanställd!"));
    }
    if ("shouldBeRenewed" in emp) {
        endDateCell.onclick = function () {
            emp.shouldBeRenewed = !emp.shouldBeRenewed;
            endDateCell.style.color = (emp.shouldBeRenewed) ? "green" : "red";
        };
        endDateCell.style.color = (emp.shouldBeRenewed) ? "green" : "red";
    }
    tr.append(nameCell, salaryCell, endDateCell);
    if (empTableBody) {
        empTableBody.append(tr);
    }
};
for (var _i = 0, employees_1 = employees; _i < employees_1.length; _i++) {
    var emp = employees_1[_i];
    _loop_1(emp);
}
