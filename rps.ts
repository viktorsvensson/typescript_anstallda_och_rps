interface winnable {
    win: () => void
}

interface loseable {
    lose: () => void
}

interface CanMakeMove {
    makeMove: () => Move
}

type Move = "STEN" | "SAX" | "PÅSE"

//
class Player implements winnable, loseable {
    name: string;
    private lives: number;

    constructor(name: string) {
        this.name = name;
        this.lives = 3;
    }

    getLives = () => this.lives;

    win = () => {
        if (this.lives < 5) {
            this.lives++
        }
    }

    lose = () => {
        this.lives--;
        if (this.lives < 1) {
            alert(this.name + ": Det var mitt sista liv, jag är ute");
            if (!("makeMove" in this)) {
                [saxBtn, stenBtn, paseBtn].forEach((btn) => btn.disabled = true)
            }
        }
    }
}


abstract class Enemy extends Player implements CanMakeMove {

    abstract makeMove: () => Move

    constructor(name: string) {
        super(name)
    }

}

const translateNumberToMove = (moveNumber: number): Move => {

    let number = Math.abs(moveNumber) % 3 as 0 | 1 | 2;

    switch (number) {
        case 0:
            return "STEN"
        case 1:
            return "SAX"
        case 2:
            return "PÅSE"
        default:
            const exhaustCheck: never = number;
            return exhaustCheck;
    }
}

class Slumpis extends Enemy {

    constructor() {
        super("Slumpis")
    }

    makeMove = (): Move => {
        const moveNumber = Math.floor(Math.random() * 3);
        return translateNumberToMove(moveNumber)
    }

}

class Vokalis extends Enemy {

    constructor() {
        super("Vokalis")
    }

    makeMove = (): Move => {
        const moveNumber = player.name
            .split("")
            .filter(char => ["a", "o", "u", "i", "e", "å", "ä", "ä"].includes(char.toLowerCase()))
            .length;

        return translateNumberToMove(moveNumber)
    }

}

class Klockis extends Enemy {

    constructor() {
        super("Klockis")
    }

    makeMove = (): Move => {
        const moveNumber = new Date().getMinutes();
        return translateNumberToMove(moveNumber);
    }

}


let player = new Player("Anonym människa");
let enemies: Enemy[] = [
    new Slumpis(),
    new Vokalis(),
    new Klockis(),
]

const lastPlayerMoveSpan = document.getElementById("last-player-move") as HTMLSpanElement;
const lastOpponentMoveSpan = document.getElementById("last-opponent-move") as HTMLSpanElement;
const lastOpponentSpan = document.getElementById("last-opponent") as HTMLSpanElement;
const nextOpponentSpan = document.getElementById("next-opponent") as HTMLSpanElement;
const livesLeftDiv = document.getElementById("lives-left") as HTMLDivElement;

const saxBtn = document.getElementById("sax-btn") as HTMLButtonElement
const stenBtn = document.getElementById("sten-btn") as HTMLButtonElement
const paseBtn = document.getElementById("pase-btn") as HTMLButtonElement

saxBtn.onclick = () => handleMove("SAX");
stenBtn.onclick = () => handleMove("STEN");
paseBtn.onclick = () => handleMove("PÅSE");

let currentOpponent = enemies[0];
nextOpponentSpan.innerText = currentOpponent.name;

const handleMove = (playerMove: Move) => {

    let opponentMove = currentOpponent.makeMove()

    lastPlayerMoveSpan.innerText = playerMove;
    lastOpponentMoveSpan.innerText = opponentMove;
    lastOpponentSpan.innerText = currentOpponent.name;


    if (playerMove === opponentMove) {
        currentOpponent = setNextOpponent(enemies)
        renderScores()
        return;
    }

    switch (playerMove) {
        case "STEN": {
            opponentMove === "SAX" ? handleOutcome(player, currentOpponent) : handleOutcome(currentOpponent, player)
            break;
        }
        case "SAX": {
            opponentMove === "PÅSE" ? handleOutcome(player, currentOpponent) : handleOutcome(currentOpponent, player)
            break;
        }
        case "PÅSE": {
            opponentMove === "STEN" ? handleOutcome(player, currentOpponent) : handleOutcome(currentOpponent, player)
            break;
        }
        default: {
            const exhaustCheck: never = playerMove;
            throw new Error("Unhandled case: " + exhaustCheck)
        }
    }

}

const handleOutcome = (winner: Player, loser: Player) => {
    winner.win()
    loser.lose()
    currentOpponent = setNextOpponent(enemies)
    renderScores()
}

const setNextOpponent = (enemies: Enemy[]) => {
    let nextOpponent = enemies
        .filter(enemy => enemy.getLives() > 0)
        .map(enemy => ({ enemy, sortValue: Math.random() }))
        .sort((a, b) => a.sortValue - b.sortValue)
        .map(({ enemy }) => enemy)[0]

    if (!nextOpponent) {
        alert("Du har besegrat alla motståndare")
    }
    return nextOpponent
}

const renderScores = () => {

    let statusNodes: (Text | HTMLBRElement)[] = [];

    for (let participant of [...enemies, player]) {
        statusNodes.push(document.createTextNode(`${participant.name}: ${participant.getLives()}`))
        statusNodes.push(document.createElement('br'))
    }

    livesLeftDiv.replaceChildren(...statusNodes)

}

renderScores()