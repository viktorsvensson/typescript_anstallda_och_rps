class Human {
    private name: string;
    private age: number;

    constructor(name: string, age: number) {
        this.name = name.toUpperCase()
        this.age = age;
    }

    increaseAge() {
        this.age += 9
    }

    setName(name: string) {
        this.name = name.toUpperCase()
    }
}

let human1 = new Human("Alice", 25)

human1.increaseAge()

human1.setName("Åke")

console.log(human1)

let list = ["bil", "båt", "cykel"];