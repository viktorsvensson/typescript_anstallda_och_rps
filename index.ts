type Employee = {
    name: string
    salary: number
    //endDate?: Date,
    //shouldBeRenewed?: boolean;
}

/* type Consultant = {
    name: string,
    salary: number
    endDate: Date,
    shouldBeRenewed: boolean;
} */

type Consultant = Employee & {
    endDate: Date,
    shouldBeRenewed: boolean;
}

let employees: (Employee | Consultant)[] = [
    { name: "Bob", salary: 32000 },
    { name: "Alice", salary: 27000, endDate: new Date(2025, 0, 1), shouldBeRenewed: false }
]

const empTableBody = document.querySelector("tbody")

for (let emp of employees) {
    const tr = document.createElement("tr");

    const nameCell = document.createElement("td");

    const salaryCell = document.createElement("td");
    salaryCell.setAttribute("contenteditable", "true")

    salaryCell.onblur = (evebt: FocusEvent) => {

        const inputedSalary = +salaryCell.innerText;

        /*emp.salary = !isNaN(inputedSalary) 
        ? inputedSalary 
        : (alert(salaryCell.innerText + " was not a valid number"), emp.salary)*/

        if (!isNaN(inputedSalary)) {
            emp.salary = inputedSalary;
        } else {
            alert(salaryCell.innerText + " was not a valid number");
        }

        salaryCell.innerText = emp.salary.toString();

    }

    const endDateCell = document.createElement("td");

    nameCell.append(document.createTextNode(emp.name))
    salaryCell.append(document.createTextNode(emp.salary.toString()))

    if ("endDate" in emp) {
        endDateCell.append(document.createTextNode(emp.endDate.toDateString()))
    } else {
        endDateCell.append(document.createTextNode("Tillsvidareanställd!"))
    }

    if ("shouldBeRenewed" in emp) {

        endDateCell.onclick = () => {
            emp.shouldBeRenewed = !emp.shouldBeRenewed;
            endDateCell.style.color = (emp.shouldBeRenewed) ? "green" : "red"
        }

        endDateCell.style.color = (emp.shouldBeRenewed) ? "green" : "red"
    }

    tr.append(nameCell, salaryCell, endDateCell)

    if (empTableBody) {
        empTableBody.append(tr)
    }

}