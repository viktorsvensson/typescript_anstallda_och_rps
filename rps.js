var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
//
var Player = /** @class */ (function () {
    function Player(name) {
        var _this = this;
        this.getLives = function () { return _this.lives; };
        this.win = function () {
            if (_this.lives < 5) {
                _this.lives++;
            }
        };
        this.lose = function () {
            _this.lives--;
            if (_this.lives < 1) {
                alert(_this.name + ": Det var mitt sista liv, jag är ute");
                if (!("makeMove" in _this)) {
                    [saxBtn, stenBtn, paseBtn].forEach(function (btn) { return btn.disabled = true; });
                }
            }
        };
        this.name = name;
        this.lives = 3;
    }
    return Player;
}());
var Enemy = /** @class */ (function (_super) {
    __extends(Enemy, _super);
    function Enemy(name) {
        return _super.call(this, name) || this;
    }
    return Enemy;
}(Player));
var translateNumberToMove = function (moveNumber) {
    var number = Math.abs(moveNumber) % 3;
    switch (number) {
        case 0:
            return "STEN";
        case 1:
            return "SAX";
        case 2:
            return "PÅSE";
        default:
            var exhaustCheck = number;
            return exhaustCheck;
    }
};
var Slumpis = /** @class */ (function (_super) {
    __extends(Slumpis, _super);
    function Slumpis() {
        var _this = _super.call(this, "Slumpis") || this;
        _this.makeMove = function () {
            var moveNumber = Math.floor(Math.random() * 3);
            return translateNumberToMove(moveNumber);
        };
        return _this;
    }
    return Slumpis;
}(Enemy));
var Vokalis = /** @class */ (function (_super) {
    __extends(Vokalis, _super);
    function Vokalis() {
        var _this = _super.call(this, "Vokalis") || this;
        _this.makeMove = function () {
            var moveNumber = player.name
                .split("")
                .filter(function (char) { return ["a", "o", "u", "i", "e", "å", "ä", "ä"].includes(char.toLowerCase()); })
                .length;
            return translateNumberToMove(moveNumber);
        };
        return _this;
    }
    return Vokalis;
}(Enemy));
var Klockis = /** @class */ (function (_super) {
    __extends(Klockis, _super);
    function Klockis() {
        var _this = _super.call(this, "Klockis") || this;
        _this.makeMove = function () {
            var moveNumber = new Date().getMinutes();
            return translateNumberToMove(moveNumber);
        };
        return _this;
    }
    return Klockis;
}(Enemy));
var player = new Player("Anonym människa");
var enemies = [
    new Slumpis(),
    new Vokalis(),
    new Klockis(),
];
var lastPlayerMoveSpan = document.getElementById("last-player-move");
var lastOpponentMoveSpan = document.getElementById("last-opponent-move");
var lastOpponentSpan = document.getElementById("last-opponent");
var nextOpponentSpan = document.getElementById("next-opponent");
var livesLeftDiv = document.getElementById("lives-left");
var saxBtn = document.getElementById("sax-btn");
var stenBtn = document.getElementById("sten-btn");
var paseBtn = document.getElementById("pase-btn");
saxBtn.onclick = function () { return handleMove("SAX"); };
stenBtn.onclick = function () { return handleMove("STEN"); };
paseBtn.onclick = function () { return handleMove("PÅSE"); };
var currentOpponent = enemies[0];
nextOpponentSpan.innerText = currentOpponent.name;
var handleMove = function (playerMove) {
    var opponentMove = currentOpponent.makeMove();
    lastPlayerMoveSpan.innerText = playerMove;
    lastOpponentMoveSpan.innerText = opponentMove;
    lastOpponentSpan.innerText = currentOpponent.name;
    if (playerMove === opponentMove) {
        currentOpponent = setNextOpponent(enemies);
        renderScores();
        return;
    }
    switch (playerMove) {
        case "STEN": {
            opponentMove === "SAX" ? handleOutcome(player, currentOpponent) : handleOutcome(currentOpponent, player);
            break;
        }
        case "SAX": {
            opponentMove === "PÅSE" ? handleOutcome(player, currentOpponent) : handleOutcome(currentOpponent, player);
            break;
        }
        case "PÅSE": {
            opponentMove === "STEN" ? handleOutcome(player, currentOpponent) : handleOutcome(currentOpponent, player);
            break;
        }
        default: {
            var exhaustCheck = playerMove;
            throw new Error("Unhandled case: " + exhaustCheck);
        }
    }
};
var handleOutcome = function (winner, loser) {
    winner.win();
    loser.lose();
    currentOpponent = setNextOpponent(enemies);
    renderScores();
};
var setNextOpponent = function (enemies) {
    var nextOpponent = enemies
        .filter(function (enemy) { return enemy.getLives() > 0; })
        .map(function (enemy) { return ({ enemy: enemy, sortValue: Math.random() }); })
        .sort(function (a, b) { return a.sortValue - b.sortValue; })
        .map(function (_a) {
        var enemy = _a.enemy;
        return enemy;
    })[0];
    if (!nextOpponent) {
        alert("Du har besegrat alla motståndare");
    }
    return nextOpponent;
};
var renderScores = function () {
    var statusNodes = [];
    for (var _i = 0, _a = __spreadArray(__spreadArray([], enemies, true), [player], false); _i < _a.length; _i++) {
        var participant = _a[_i];
        statusNodes.push(document.createTextNode("".concat(participant.name, ": ").concat(participant.getLives())));
        statusNodes.push(document.createElement('br'));
    }
    livesLeftDiv.replaceChildren.apply(livesLeftDiv, statusNodes);
};
renderScores();
